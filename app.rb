require 'sinatra'
require 'date'
require 'raw_date_detector'

def raw_year_common(year)
  @raw_lead = RawDateDetector.raw_year?(year) ? "Yes" : "No"
  @raw_message = RawDateDetector.raw_year?(year) ? "It's one life, the raw life" : "There is many lives, Most of them unraw"
  @year_arr = RawDateDetector.raw_arr(year)
  @first_section = @year_arr[0] + @year_arr[1]
  @second_section = @year_arr[2] + @year_arr[3]
  @solution = @first_section / @second_section
end

get '/' do
  raw_year_common(Date.today.year)
  erb :index
end

get '/:year' do
  raw_year_common(params['year'])
  erb :index
end
