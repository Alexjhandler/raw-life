# Raw Life

Code repo of the raw life web site at www.isitarawyear.com

a raw year is defined as any year that is is divisible by itself to equal one. For example:

### 2020
- `2 + 0 = 2`
- `2 + 0 = 2`
- `2 / 2 = 1`
> it's one life, **the raw life**

### 1999 (a non raw year)
- `1 + 9 = 10`
- `9 + 9 = 18`
- `10 / 18 = 0.5555555555555556`
> It's not one life, and its certainly not a **raw life**

### Running the Application

To run, clone this repo then:
- `cd raw-life`
- `bundle install`
- `rerun app.rb`
- :tada:
---
### Running tests
`bundle exec rake`


This is a silly site made for silly reasons. Don't forget to be raw.
